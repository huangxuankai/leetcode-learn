package main

import (
    "sort"
    "fmt"
)

func arrayPairSum(nums []int) int {
    sort.Ints(nums)
    length := len(nums)
    sum := 0
    for i := 0; i < length/2; i++ {
        sum += nums[2*i]
    }
    return sum
}

func arrayPairSum2(nums []int) int {
    for i := 0; i < len(nums)-1; i++ {
        for j := i+1; j < len(nums); j++ {
            if nums[i] > nums[j] {
                nums[i], nums[j] = nums[j], nums[i]
            }
        }
    }
    sum := 0
    for i := 0; i < len(nums)/2; i++ {
        sum += nums[2*i]
    }
    return sum
}

func arrayPairSum3(nums []int) int {

    numMap := make(map[int]int)

    for i := range nums {
        numMap[nums[i]]++
    }
    sum := 0
    flag := 0
    for i := -10000; i <= 10000; i++ {
        for j := 0; j < numMap[i]; j++ {
            if flag % 2 == 0 {
                sum += i
            }
            flag++
        }
    }
    return sum
}



func main() {
    nums := []int{1, 4, 3, 2, 9, 2, 1, 7}
    //fmt.Println(arrayPairSum(nums))
    //fmt.Println(arrayPairSum2(nums))
    fmt.Println(arrayPairSum3(nums))
}
