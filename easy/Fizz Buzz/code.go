package main

import "fmt"

func fizzBuzz(n int) []string {

	var output []string
	for i := 1; i <= n; i++ {
		if i % 15 == 0 {
			output = append(output, "FizzBuzz")
			continue
		} else {
			if i % 5 == 0 {
				output = append(output, "Buzz")
				continue
			}
			if i % 3 == 0 {
				output = append(output, "Fizz")
				continue
			}
			output = append(output, fmt.Sprintf("%v", i))
		}
	}
	fmt.Println(output)
	return output
}

func main() {
	fizzBuzz(15)
}
