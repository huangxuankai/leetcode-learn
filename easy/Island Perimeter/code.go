package main

import "fmt"

func islandPerimeter(grid [][]int) int {

    islandCount := 0
    connectCount := 0
    row := len(grid)
    col := len(grid[0])
    for i := range grid {
        for j := range grid[i] {
            if grid[i][j] == 1 {
                islandCount++
                if j < col - 1 && grid[i][j] == grid[i][j+1] {
                    connectCount++
                }
            }
        }
    }

    for j := 0; j < col; j++ {
        for i := 0; i < row - 1; i++ {
            if grid[i][j] == 1 && grid[i][j] == grid[i+1][j] {
                connectCount++
            }
        }
    }
    return islandCount * 4 - connectCount * 2
}

func main() {
    grid := [][]int{
        {0,1,0,0},
        {1,1,1,0},
        {0,1,0,0},
        {1,1,0,0},
    }
    fmt.Println(islandPerimeter(grid))
}
