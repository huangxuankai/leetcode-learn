package main

import "fmt"

func calculateJewels(jewels string, stones string) int {

    count := 0
    for i := range jewels {
        for j := range stones {
            if jewels[i] == stones[j] {
                count++
            }
        }
    }
    fmt.Printf("Input: J = %v, S = %v \n", jewels, stones)
    fmt.Printf("Output: %v \n", count)
    return count

}

func calculateJewels2(jewels string, stones string) int {
    count := 0
    stacks := make(map[int32]int)

    for _, v := range stones {
        stacks[v]++
    }
    for _, v := range jewels {
        count += stacks[v]
    }

    fmt.Printf("Input: J = %v, S = %v \n", jewels, stones)
    fmt.Printf("Output: %v \n", count)
    return count
}


func main() {
    j := "aA"
    s := "aAAnnnmmm"

    calculateJewels(j, s)
    calculateJewels2(j, s)

}
