# -*- coding: utf-8 -*-
""" By Daath """


def calculate_jewels(jewels, stones):
    count = 0
    for i in jewels:
        for j in stones:
            if i == j:
                count += 1
    return count


def calculate_jewels2(jewels, stones):
    count = 0
    stacks = {}
    for i in stones:
        stacks[i] = stacks.get(i, 0)
        stacks[i] += 1

    for i in jewels:
        count += stacks.get(i, 0)
    return count


# References from other
def calculate_jewels3(jewels, stones):
    count = len(filter(lambda s: s in jewels, stones))
    count = sum(map(jewels.count, stones))
    count = sum(s in jewels for s in stones)
    count = len([s for s in stones if s in jewels])
    return count



if __name__ == '__main__':
    j = 'aAv'
    s = 'aAAnnnn'
    print(calculate_jewels3(j, s))
