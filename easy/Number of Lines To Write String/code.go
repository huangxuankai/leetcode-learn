package main

func numberOfLines(widths []int, S string) []int {

    row := 1
    sum := 0
    for i := range S {
        index := S[i] - 'a'
        sum += widths[index]
        if sum > 100 {
            sum = 0 + widths[index]
            row++
        }
    }
    return []int{row, sum}
}

func main() {
    widths := []int{4,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10}
    str := "bbbcccdddaaa"
    numberOfLines(widths, str)

}
