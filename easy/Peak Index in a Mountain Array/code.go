package main

import (
    "fmt"
    "time"
)

func peakIndexInMountainArray(A []int) int {
    var i int
    for i = 0; i < len(A)-1; {
        if A[i] < A[i+1] {
            i++
        } else {
            break
        }
    }
    fmt.Println("i:", i)
    return i
}

func peakIndexInMountainArray2(A []int) int {
    var maxIndex, i, j int
    for i, j = 0, len(A)-1; i < j; {
        if A[i] < A[i+1] {
            i++
        } else {
            maxIndex = i
            break
        }
        if A[j] < A[j-1] {
            j--
        } else {
            maxIndex = j
            break
        }

    }
    if i == j {
        maxIndex = i
    }
    fmt.Println("maxidex: ", maxIndex)
    return maxIndex
}

// some problem need to solution
func peakIndexInMountainArray3(A []int) int {
    var low, high, mid int
    low, high = 0, len(A)-1
    for low <= high {
        mid = (low + high) / 2
        if A[mid-1] < A[mid] && A[mid] > A[mid+1] {
            break
        }
        if A[mid-1] < A[mid] && A[mid] < A[mid+1] {
            low = mid
        }
        if A[mid-1] > A[mid] && A[mid] > A[mid+1] {
            high = mid
        }
    }
    fmt.Println(mid)
    return mid
}

func main() {
    var start, end int
    A := []int{0, 2, 3, 4, 26, 29, 6, 0}
    //start = time.Now().Nanosecond()
    //peakIndexInMountainArray(A)
    //end = time.Now().Nanosecond()
    //fmt.Println("time: ", (end-start)/1000)
    //
    //start = time.Now().Nanosecond()
    //peakIndexInMountainArray2(A)
    //end = time.Now().Nanosecond()
    //fmt.Println("time: ", (end-start)/1000)

    start = time.Now().Nanosecond()
    peakIndexInMountainArray3(A)
    end = time.Now().Nanosecond()
    fmt.Println("time: ", (end-start)/1000)

}
