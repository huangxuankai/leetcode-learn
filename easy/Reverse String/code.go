package main

import "fmt"

func reverseStrings(s string) string {

    newString := []rune(s)
    for start, end := 0, len(newString)-1; start < end; start, end = start + 1, end-1 {
        newString[start], newString[end] = newString[end], newString[start]
    }
    return string(newString)
}

func main() {
    str := ""
    fmt.Println(reverseStrings(str))
}
