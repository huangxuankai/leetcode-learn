package main

func reverseWords(s string) string {
    if s == "" {
       return ""
    }
    var i, start, end int
    var spiltStr [][]rune
    for i = range s {
        if s[i] == ' ' {
            spiltStr = append(spiltStr, []rune(s[start:i]))
            start = i+1
        }
    }
    if s[i] != ' ' {
        spiltStr = append(spiltStr, []rune(s[start:]))
    }

    var newS string
    for _, str := range spiltStr {
        for start, end = 0, len(str)-1; start <= end; start, end = start+1, end-1 {
            str[start], str[end] = str[end], str[start]
        }
        newS = newS + string(str) + " "
    }
    newS = newS[:len(newS)-1]
    return newS
}

func main() {
    S := ""
    reverseWords(S)
}
