package main

import "fmt"

/**
 * Definition for a binary tree node.
 */
type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}

func searchBST(root *TreeNode, val int) *TreeNode {
    if root == nil {
        return nil
    }
    if root.Val == val {
        return root
    }
    left := searchBST(root.Left, val)
    right := searchBST(root.Right, val)
    if left == nil {
        if right == nil {
            return nil
        } else {
            return right
        }
    } else {
        return left
    }
}

func initBinaryTree() (root *TreeNode) {
    root = new(TreeNode)
    root.Val = 4
    left := &TreeNode{}
    left.Val = 2

    right := &TreeNode{}
    right.Val = 7

    root.Left = left
    root.Right = right

    left = &TreeNode{}
    left.Val = 1

    right = &TreeNode{}
    right.Val = 3

    root.Left.Left = left
    root.Left.Right= right
    return
}

//
func preOrderTree(root *TreeNode) {

    if root == nil {
        return
    }
    fmt.Printf("%v ", root.Val)
    preOrderTree(root.Left)
    preOrderTree(root.Right)
}
func inOrderTree(root *TreeNode) {

    if root == nil {
        return
    }
    inOrderTree(root.Left)
    fmt.Printf("%v ", root.Val)
    inOrderTree(root.Right)
}
func postOrderTree(root *TreeNode) {

    if root == nil {
        return
    }
    postOrderTree(root.Left)
    postOrderTree(root.Right)
    fmt.Printf("%v ", root.Val)
}

func main() {
    root := initBinaryTree()
    targetNode := searchBST(root, 2)
    preOrderTree(targetNode)
}
