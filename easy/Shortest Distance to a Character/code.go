package main

func shortestToChar(S string, C byte) []int {
    var indexs []int
    for i := range S {
        if S[i] == C {
            indexs = append(indexs, i)
        }
    }
    strLength := len(S)
    output := make([]int, strLength)

    for j := 0; j < indexs[0]; j++ {
        output[j] = indexs[0] - j
    }
    var i = 0
    for ;i < len(indexs)-1; i++ {
        for start, end := indexs[i], indexs[i+1]; start <= end; start, end = start+1, end-1 {
            output[start] = start - indexs[i]
            output[end] = indexs[i+1] - end
        }
    }
    for j := indexs[i]; j < strLength; j++ {
        output[j] = j - indexs[i]
    }
    return output
}


func main() {
    S := "loveleetcode"
    C := byte('e')
    shortestToChar(S, C)

}
