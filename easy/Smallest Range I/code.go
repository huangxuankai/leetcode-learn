package main

import "fmt"

func smallestRangeI(A []int, K int) int {

    min, max := A[0], A[0]
    for i := range A {
        if min > A[i] {
            min = A[i]
        }
        if max < A[i] {
            max = A[i]
        }
    }
    fmt.Println(min, max)
    arrDiffer := max - min
    KDiffer := 2 * K
    if KDiffer < arrDiffer {
        return arrDiffer - KDiffer
    }
    return 0

}

func main() {
    A := []int{2, 7, 2}
    K := 1
    fmt.Println(smallestRangeI(A, K))

}
