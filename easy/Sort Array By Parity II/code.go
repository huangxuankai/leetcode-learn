package main

import "fmt"

func sortArrayByParityII(A []int) []int {

    even, odd := 0, 1

    newArray := make([]int, len(A))

    for i := range A {
        if A[i] % 2 == 0 {
            newArray[even] = A[i]
            even += 2
        } else {
            newArray[odd] = A[i]
            odd += 2
        }
    }
    return newArray
}

func sortArrayByParityII2(A []int) []int {

    pre := -1
    for i := range A {
        if (A[i] + i) % 2 != 0 {
            if pre == -1 {
                pre = i
            } else {
                A[pre], A[i] = A[i], A[pre]
                pre = -1
            }
        }
    }
    return A
}

func main() {
    A := []int{4, 2, 5, 7, 9, 0, 3, 8}
    fmt.Println(sortArrayByParityII(A))
    fmt.Println(sortArrayByParityII2(A))
}


