package main

import (
    "fmt"
    "math"
)

func stringToInt(intString string) int {
    strMapInt := map[rune]int{
        48: 0,
        49: 1,
        50: 2,
        51: 3,
        52: 4,
        53: 5,
        54: 6,
        55: 7,
        56: 8,
        57: 9,
    }
    length := len(intString)-1
    sum := 0
    for i, v := range intString{
        if v >= 48 && v <= 57 {
            sum = sum + strMapInt[v] * int(math.Pow10(length-i))
        } else {
            sum /= 10
        }
    }
    return sum
}

func subdomainVisits(cpdomains []string) []string {

    stack := make(map[string]int)
    for _, cpdomain := range cpdomains {
        var domainCount int
        var domainString string
        for i := range cpdomain {
            if cpdomain[i] == ' ' {
                domainCount = stringToInt(cpdomain[:i])
                domainString = cpdomain[i+1:]
            }
        }
        stack[domainString] += domainCount
        for i := range domainString {
            if domainString[i] == '.' {
                stack[domainString[i+1:]] += domainCount
            }
        }
    }
    var output []string
    for k, v := range stack {
        output = append(output, fmt.Sprintf("%v %v", v, k))
    }
    return output
}


func main() {
    strs := []string{"9001 discuss.leetcode.com"}
    subdomainVisits(strs)
}
