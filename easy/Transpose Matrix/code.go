package main

func transpose(A [][]int) [][]int {

    colLength := len(A[0])
    rowLength := len(A)
    transposeArray := make([][]int, colLength)
    for col := 0; col < colLength; col++ {
        tmp := make([]int, rowLength)
        for row := 0; row < rowLength; row++ {
            tmp[row] = A[row][col]
        }
        transposeArray[col] = tmp
    }
    return transposeArray

}


func main() {

    A := [][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
    transpose(A)
    A = [][]int{{1, 2, 3}, {4, 5, 6}}
    transpose(A)

}
