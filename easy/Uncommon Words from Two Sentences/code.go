package main

func uncommonFromSentences(A string, B string) []string {
    var output []string
    commonWords := make(map[string]int)
    preIdx := 0
    lengthA := len(A)
    lengthB := len(B)
    for i := 0; i <= lengthA; i++ {
        if i != lengthA && A[i] == ' ' {
            commonWords[A[preIdx:i]]++
            preIdx = i+1
        }
        if i == lengthA {
            commonWords[A[preIdx:]]++
        }
    }
    preIdx = 0
    for i := 0; i <= lengthB; i++ {
        if i != lengthB && B[i] == ' ' {
            commonWords[B[preIdx:i]]++
            preIdx = i+1
        }
        if i == lengthB {
            commonWords[B[preIdx:]]++
        }
    }
    for key := range commonWords {
        if commonWords[key] == 1 {
            output = append(output, key)
        }
    }
    return output

}

func main() {
    A := "this apple is sweet"
    B := "this apple is sour"
    uncommonFromSentences(A, B)

}
