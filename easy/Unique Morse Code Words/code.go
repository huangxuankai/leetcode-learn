package main

import (
	"encoding/json"
	"fmt"
)

func morseCodeMap() map[rune]string {

	morseCodeStr := `[".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--","-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."]`

	var morseCodeList []string
	morseCodeMap := make(map[rune]string)

	err := json.Unmarshal([]byte(morseCodeStr), &morseCodeList)
	if err != nil {
		return nil
	}
	for i, v := range morseCodeList {
		morseCodeMap[rune(i + 'a')] = v
	}
	return morseCodeMap
}

func uniqueMorseRepresentations(words []string) int {

	morseCodeMap := morseCodeMap()

	differ := make(map[string]int)

	for i := range words {
		str := ""
		for _, c := range words[i] {
			str += morseCodeMap[c]
		}
		differ[str]++
	}

	return len(differ)

}

func main() {

	words := []string{"gin", "zen", "gig", "msg", "sdad"}
	fmt.Println(uniqueMorseRepresentations(words))
	morseCodeMap()
}
